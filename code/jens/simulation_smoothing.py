# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 09:30:53 2021

@author: jensb
"""
import pandas as pd
import numpy as np
import seaborn as sns
import yfinance as yf
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import minimize
import statsmodels.api as sm

data = pd.read_pickle("../../data/Nile_daan.pkl")

var_eps = 15099
var_eta = 1469.1

# # for external dataset
# var_eps = 87.84330058
# var_eta = 8.38515049

# initial values
a_ini = 0
P_ini = 10**7
confidence = 0.9

def kalman_filter(data, var_eps=var_eps, var_eta=var_eta, a_ini=a_ini, P_ini=P_ini):
    ''' used to filter data using the recursions as discussed in the report''' 
    
    y = np.asarray(data)
    a, K, v, F, P = [np.zeros(y.size) for i in range(5)] 
    # initialize
    a[0] = a_ini
    P[0] = P_ini
    v[0] = y[0] - a[0]
    F[0] = P[0] + var_eps
    K[0] = P[0] / F[0]

    # recursions
    for t in range(1, y.size):
        # if data is missing
        if np.isnan(y[t-1]):
            a[t] = a[t-1]
            P[t] = P[t-1] + var_eta
            F[t] = P[t]   + var_eps
        else:
            # state update
            a[t] = a[t-1] + K[t-1]*v[t-1]
            P[t] = P[t-1]*(1-K[t-1]) + var_eta
            v[t] = y[t] - a[t]
            F[t] = P[t] + var_eps
            K[t] = P[t] / F[t]
        
    return [pd.Series(i, index=data.index) for i in [v, F, K, a, P]]

def state_smoothing(data, var_eps=var_eps, var_eta=var_eta, a_ini=a_ini, P_ini=P_ini):
    ''' used to smooth the state using the backward recursions as discussed in the report'''

    y = np.asarray(data)
    # first apply kalman filter from above on data
    vals = kalman_filter(data)
    v, F, K, a, P = [np.asarray(val.fillna(0)) for val in vals]
    r, alpha, N, V = [np.zeros(y.size) for i in range(4)]
    L = 1 - K

    # recursion 
    for t in range(y.size-1, 0, -1):
        # if data is missing
        if np.isnan(y[t-1]):
            # take previous value
            r[t-1]   = r[t]
            N[t-1]   = N[t]
            # smoothed state update
            alpha[t] = a[t] + P[t]*r[t-1]
            V[t]     = P[t] - (P[t]**2)*N[t-1]
        else:
            # calculate cumulant variables
            r[t-1]   = v[t]/F[t] + L[t]*r[t]
            N[t-1]   = 1/F[t] + (L[t]**2)*N[t]
            # smoothed state update
            alpha[t] = a[t] + P[t]*r[t-1]
            V[t]     = P[t] - (P[t]**2)*N[t-1]
    # last loop to make length of vectors 101
    r = np.insert(r, 0, v[t-1]/F[t-1] + L[t-1]*r[t-1])
    N = np.insert(N, 0, 1/F[t-1] + (L[t-1]**2)*N[t-1])
    alpha[0] = a[0] + P[0]*r[0]
    V[0]     = P[0] - (P[0]**2)*N[0]
    alpha    = np.insert(alpha, 0, np.nan)
    V        = np.insert(V, 0, np.nan)

    return [pd.Series(i, index=data.index.insert(0, pd.to_datetime(int(data.index[0])-1, format='%Y').year)) for i in [r, alpha, N, V]]



# %%
# smoothing simulation
v, F, K, a, P  = kalman_filter(data)
r, alpha, N, V = state_smoothing(data)

# x = np.log(np.random.normal(size=10000)**2)
# plt.hist(x, bins=100)

# %%
# simulation smoothing
a1 = np.random.normal(a.iloc[0], np.sqrt(P.iloc[0]))
eta = np.random.normal(scale=np.sqrt(var_eta), size=99)
eps = np.random.normal(scale=np.sqrt(var_eps), size=100)

a_plus = np.insert(eta.cumsum(), 0, a1)
y_plus = a_plus + eps

# apply kfs only once
_, correction, _, _ = state_smoothing(data - y_plus)

plt.plot(a_plus[1:] + correction[2:], label = 'a tilde')
plt.plot(a[1:], label='real')
plt.legend()