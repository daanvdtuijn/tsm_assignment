import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from statsmodels.graphics.tsaplots import plot_acf
import scipy.stats as stats
import seaborn as sea
from statsmodels.graphics.gofplots import qqplot

data = pd.read_pickle('../../data/Nile.pkl')

#set constants
sigma_eps = 15099
sigma_eta = 1469.1
a_ini = 0
P_ini = 10**7

#########################
###### Figure 2.1 #######
#########################


def kalman_filter(y, a_ini, P_ini, sigma_eps, sigma_eta):
    "forward recursion algorithm"
    
    a, K, v, F, P = np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y))
    
    a[0] = a_ini
    P[0] = P_ini
    
    for t in range(0, len(y)-1):
        
        #based on T1S19 - Tutorial 1 Slide 19
        v[t] = y[t] - a[t]
        F[t] = P[t] + sigma_eps
        K[t] = P[t] / F[t] 
        a[t+1] = a[t] + K[t]*v[t]
        P[t+1] = ( (P[t]*sigma_eps) / (P[t] + sigma_eps) ) + sigma_eta
        
        #such that last value is not 0
        F[t+1] = P[t+1] + sigma_eps
    
    return a, K, v, F, P

# function to calculate boundaries -> does not work as the boundaries are too far away from the filtered state,
     #comparing the plots with the book      
def calc_boundaries(y):
    
    boundaries = []
    for t in range(len(y)):
        boundaries.append(1.645 * np.sqrt(y[t]))    
        
    return boundaries


a, K, v, F, P = kalman_filter(data, a_ini, P_ini, sigma_eps, sigma_eta)  
boundaries = calc_boundaries(P)

fig1, ax = plt.subplots(2,2, figsize=(20,10))
ax[0, 0].scatter(data.index, data, color='black')
ax[0, 0].plot(a[1:], color='black')
ax[0, 0].plot(a[1:] + boundaries[1:], color='grey')
ax[0, 0].plot(a[1:] - boundaries[1:], color='grey')
ax[0, 1].plot(P[1:], color='black')
ax[1, 0].plot(v[1:], color='black') 
ax[1, 0].axhline(color='black', alpha = 0.2) 
ax[1, 1].plot(F[1:], color='black')

[l.set_visible(False) for (i,l) in enumerate(ax[0,0].xaxis.get_ticklabels()) if i % 10 != 0]


#########################
###### Figure 2.2 #######
#########################

def state_smoothing_recursion(y, a_ini, P_ini, sigma_eps, sigma_eta):
    "backwards recursion algorithm"
    
    a, K, v, F, P = kalman_filter(y, a_ini, P_ini, sigma_eps, sigma_eta)  
    r, alpha, N, V, L = np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y))
    
    #recursive loop
    for t in range(len(y)-1, 0, -1):
              
        #based on formulas in book/overleaf - correct way including L(t)
        L[t] = 1 - K[t]
        r[t-1] = ( v[t] / F[t] ) + ( L[t] * r[t])
        N[t-1] = (1 / F[t]) + ( L[t]**2 * N[t])
        alpha[t] = a[t] + (P[t] * r[t-1])
        V[t] = P[t] - (P[t]**2 * N[t-1])
        
    
    return r, alpha, N, V

r, alpha, N, V = state_smoothing_recursion(data, a_ini, P_ini, sigma_eps, sigma_eta)
boundaries = calc_boundaries(V)

fig2, ax = plt.subplots(2,2, figsize=(20,10))
ax[0, 0].scatter(data.index, data, color='black')
ax[0, 0].plot(alpha[1:], color='black')
ax[0, 0].plot(alpha[1:] + boundaries[1:], color='grey')
ax[0, 0].plot(alpha[1:] - boundaries[1:], color='grey')
ax[0, 1].plot(V[1:], color='black')
ax[1, 0].plot(r[1:], color='black') 
ax[1, 0].axhline(color='black', alpha = 0.2) 
ax[1, 1].plot(N[1:], color='black')




#########################
###### Figure 2.3 #######
#########################


def disturbance_smoothing_recursion(y, a_ini, P_ini, sigma_eps, sigma_eta):
    
    a, K, v, F, P = kalman_filter(y, a_ini, P_ini, sigma_eps, sigma_eta)  
    r, alpha, N, V = state_smoothing_recursion(data, a_ini, P_ini, sigma_eps, sigma_eta)
    
    D, eps, var_eps, eta, var_eta = np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y)), np.zeros(len(y))
    
    #recursive loop
    for t in range(len(y)-1, 0, -1):
        
        #based on formulas in book/overleaf
        D[t] = (1 / F[t]) + (K[t]**2 * N[t])
        eps[t] = y.iloc[t] - alpha[t]
        var_eps[t] = sigma_eps - (sigma_eps**2 * D[t])
        eta[t] = sigma_eta * r[t]
        var_eta[t] = sigma_eta - (sigma_eta**2 * N[t])
    
    return eps, var_eps, eta, var_eta

eps, var_eps, eta, var_eta = disturbance_smoothing_recursion(data, a_ini, P_ini, sigma_eps, sigma_eta)

fig3, ax = plt.subplots(2,2, figsize=(20,10))
ax[0, 0].plot(eps[1:], color='black')
ax[0, 0].axhline(color='black', alpha = 0.2) 
#take sqrt of var as it is given in assignment
ax[0, 1].plot(np.sqrt(var_eps[1:]), color='black')
ax[1, 0].plot(eta[1:], color='black') 
ax[1, 0].axhline(color='black', alpha = 0.2) 
#take sqrt of var as it is given in assignment
ax[1, 1].plot(np.sqrt(var_eta[1:]), color='black')



#########################
###### Figure 2.5 #######
#########################



#########################
###### Figure 2.6 #######
#########################



#########################################
##### Maximum Likilihood estimation #####
#########################################

def likelihood_function(params, y):
    
    sigma_eps = params[0]
    sigma_eta = params[1]
    
    a, K, v, F, P = kalman_filter(y, a_ini, P_ini, sigma_eps, sigma_eta)     
    
    negLL = np.sum(-(-(1/2)*(np.log(F)+(np.power(v,2)/F))))
    
    return(negLL)

initial = np.array([1000,1000])
results = minimize(likelihood_function, initial, (data), method = 'Nelder-Mead', options={'disp': False})

sigma_eps_ML, sigma_eta_ML = (results.x[0], results.x[1])

#########################
###### Figure 2.7 #######
#########################


a, K, v, F, P = kalman_filter(data, a_ini, P_ini, sigma_eps_ML, sigma_eta_ML)
e_t = v[1:] / np.sqrt(F[1:])

fig7, ax = plt.subplots(2,2, figsize=(20,10))
ax[0, 0].plot(e_t[1:], color='black')
ax[0, 0].axhline(color='black', alpha = 0.2) 
sea.distplot(e_t, hist=True, kde=True,  bins=13, color = 'black', hist_kws={'edgecolor':'black'}, ax = ax[0,1])
qqplot(e_t, ax = ax[1, 0], line ='s')
plot_acf(e_t, lags =10, ax = ax[1, 1])


#########################
###### Figure 2.8 #######
#########################

eps, var_eps, eta, var_eta = disturbance_smoothing_recursion(data, a_ini, P_ini, sigma_eps_ML, sigma_eta_ML)

ut_star = eps / np.std(eps)
rt_star = eta / np.std(eta)

fig8, ax = plt.subplots(2,2, figsize=(20,10))
ax[0, 0].plot(ut_star, color='black')
ax[0, 0].axhline(color='black', alpha = 0.2) 
sea.distplot(ut_star, hist=True, kde=True,  bins=13, color = 'black', hist_kws={'edgecolor':'black'}, ax = ax[0,1])
ax[1, 0].plot(rt_star, color='black')
ax[1, 0].axhline(color='black', alpha = 0.2) 
sea.distplot(rt_star, hist=True, kde=True,  bins=13, color = 'black', hist_kws={'edgecolor':'black'}, ax = ax[1,1])




