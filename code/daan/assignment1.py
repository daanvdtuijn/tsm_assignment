import pandas as pd
import numpy as np
import seaborn as sns
import yfinance as yf
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import minimize
import statsmodels.api as sm

data = pd.read_pickle("../../data/Nile_daan.pkl")

# %%
# external dataset
data = pd.read_pickle('../../data/norway_crop.pkl')

fig, axs = plt.subplots(2,2,figsize=(12,7))

axs[0,0].set_title('i)')
axs[0,0].plot(data, color='black', lw=1)
axs[0,1].set_title('ii)')
axs[0,1].bar(range(1,11), acf(data,11)[1:], color='grey', edgecolor='black', width=.5)
axs[0,1].set_ylim([-1,1])
axs[0,1].axhline(0, color='black', lw=.9)
axs[0,1].xaxis.set_ticks(np.arange(0, 11))
axs[0,1].yaxis.set_ticks(np.arange(-1, 1.5, 0.5))
axs[1,0].set_title('iii)')
axs[1,0].plot(data.diff().dropna(), color='black', lw=1)
axs[1,0].axhline(0, color='black', lw=.9)
axs[1,0].set_ylim([-42,42])
axs[1,1].set_title('iv)')
axs[1,1].bar(range(1,11), acf(data.diff().dropna(),11)[1:], color='grey', edgecolor='black', width=.5)
axs[1,1].axhline(0, color='black', lw=.9)
axs[1,1].set_ylim([-1,1])
axs[1,1].xaxis.set_ticks(np.arange(0, 11))
axs[1,1].yaxis.set_ticks(np.arange(-1, 1.5, 0.5))

fig.tight_layout()
fig.savefig('../../plots/descriptives.png')

# functions
# %%
# set global constants
var_eps = 15099
var_eta = 1469.1

# # for external dataset
# var_eps = 87.84330058
# var_eta = 8.38515049

# initial values
a_ini = 0
P_ini = 10**7
confidence = 0.9


def confidence_interval(variances, confidence=confidence):
    ''' used to calculate confidence bounds with given variance and confidence level''' 
    crit_val = norm.ppf(confidence + (1-confidence)/2)
    return [(crit_val*np.sqrt(var)) for var in variances]

def acf(x, lags=11):
    ''' used to calcualte autocorrelation with given sample and lags'''
    return np.array([1]+[np.corrcoef(x[:-i], x[i:])[0,1]  \
        for i in range(1, lags)])

def kalman_filter(data, var_eps=var_eps, var_eta=var_eta, a_ini=a_ini, P_ini=P_ini):
    ''' used to filter data using the recursions as discussed in the report''' 
    
    y = np.asarray(data)
    a, K, v, F, P = [np.zeros(y.size) for i in range(5)] 
    # initialize
    a[0] = a_ini
    P[0] = P_ini
    v[0] = y[0] - a[0]
    F[0] = P[0] + var_eps
    K[0] = P[0] / F[0]

    # recursions
    for t in range(1, y.size):
        # if data is missing
        if np.isnan(y[t-1]):
            a[t] = a[t-1]
            P[t] = P[t-1] + var_eta
            F[t] = P[t]   + var_eps
        else:
            # state update
            a[t] = a[t-1] + K[t-1]*v[t-1]
            P[t] = P[t-1]*(1-K[t-1]) + var_eta
            v[t] = y[t] - a[t]
            F[t] = P[t] + var_eps
            K[t] = P[t] / F[t]
        
    return [pd.Series(i, index=data.index) for i in [v, F, K, a, P]]

def state_smoothing(data, var_eps=var_eps, var_eta=var_eta, a_ini=a_ini, P_ini=P_ini):
    ''' used to smooth the state using the backward recursions as discussed in the report'''

    y = np.asarray(data)
    # first apply kalman filter from above on data
    vals = kalman_filter(data)
    v, F, K, a, P = [np.asarray(val.fillna(0)) for val in vals]
    r, alpha, N, V = [np.zeros(y.size) for i in range(4)]
    L = 1 - K

    # recursion 
    for t in range(y.size-1, 0, -1):
        # if data is missing
        if np.isnan(y[t-1]):
            # take previous value
            r[t-1]   = r[t]
            N[t-1]   = N[t]
            # smoothed state update
            alpha[t] = a[t] + P[t]*r[t-1]
            V[t]     = P[t] - (P[t]**2)*N[t-1]
        else:
            # calculate cumulant variables
            r[t-1]   = v[t]/F[t] + L[t]*r[t]
            N[t-1]   = 1/F[t] + (L[t]**2)*N[t]
            # smoothed state update
            alpha[t] = a[t] + P[t]*r[t-1]
            V[t]     = P[t] - (P[t]**2)*N[t-1]
    # last loop to make length of vectors 101
    r = np.insert(r, 0, v[t-1]/F[t-1] + L[t-1]*r[t-1])
    N = np.insert(N, 0, 1/F[t-1] + (L[t-1]**2)*N[t-1])
    alpha[0] = a[0] + P[0]*r[0]
    V[0]     = P[0] - (P[0]**2)*N[0]
    alpha    = np.insert(alpha, 0, np.nan)
    V        = np.insert(V, 0, np.nan)

    return [pd.Series(i, index=data.index.insert(0, pd.to_datetime(int(data.index[0])-1, format='%Y').year)) for i in [r, alpha, N, V]]

def disturbance_smoothing(data, var_eps=var_eps, var_eta=var_eta, a_ini=a_ini, P_ini=P_ini):
    # first apply kalman filter
    vals = kalman_filter(data)  
    v, F, K, a, P = [np.asarray(val) for val in vals]
    # then apply smoothing recursion
    vals = state_smoothing(data)
    r, alpha, N, V = [np.asarray(val[1:]) for val in vals] # get rid of first value at 1870
    y = np.asarray(data)
    # create empty arrays
    D, eps, cvar_eps, eta, cvar_eta = [np.zeros(y.size) for i in range(5)] 
    # recucursions for disturbance smoothing
    D        = 1/F + (K**2)*N
    eps      = y - alpha
    cvar_eps = var_eps - (var_eps**2)*D
    eta      = var_eta*r
    cvar_eta = var_eta - (var_eta**2)*N
    return [pd.Series(i, index=data.index) for i in [eps, cvar_eps, eta, cvar_eta]]

def llf(params, y):
    # function that has to be optimized
    n = y.size
    var_eps = params[0]
    var_eta = params[1]
    v, F, K, a, P = kalman_filter(y, var_eps, var_eta)     
    # log likelihood function
    ll = -(n/2)*np.log(2*np.pi) - 0.5*np.sum( np.log(F[1:]) + (v[1:]**2)/F[1:] ) 
    return -ll # negative because we use minimize


# Figure 2.1
# %%
v, F, K, a, P = kalman_filter(data)
ci = confidence_interval(P[1:])

fig, axs = plt.subplots(2,2,figsize=(12,7), sharex=True)

axs[0,0].set_title('i)')
axs[0,0].plot(data, '.', c='black')
axs[0,0].plot(a[1:], c='grey')
axs[0,0].plot(a[1:] + ci, c='grey', lw=.5)
axs[0,0].plot(a[1:] - ci, c='grey', lw=.5)
axs[0,1].set_title('ii)')
axs[0,1].plot(P[1:], c='black')
axs[1,0].set_title('iii)')
axs[1,0].plot(v[1:], c='black', lw=.9)
axs[1,0].axhline(0, c='grey')
axs[1,1].set_title('iv)')
axs[1,1].plot(F[1:], c='black')

fig.tight_layout()
fig.savefig('../../plots/fig21.png', dpi=500)
# fig.savefig('../../plots/fig31.png', dpi=500)


# Figure 2.2
# %%
r, alpha, N, V = state_smoothing(data)
ci = confidence_interval(V[1:])

fig, axs = plt.subplots(2,2, figsize=(12,7), sharex=True)
axs[0,0].set_title('i)')
axs[0,0].plot(data, '.', c='black')
axs[0,0].plot(alpha[1:], color='grey')
axs[0,0].plot(alpha[1:] + ci, c='grey', lw=.5)
axs[0,0].plot(alpha[1:] - ci, c='grey', lw=.5)
axs[0,1].set_title('ii)')
axs[0,1].plot(V[1:], c='black')
axs[1,0].set_title('iii)')
axs[1,0].plot(r[1:-1], c='black', lw=.9)
axs[1,0].axhline(0, c='grey')
axs[1,1].set_title('iv)')
axs[1,1].plot(N[1:-1], c='black')
fig.tight_layout()
# fig.savefig('../../plots/fig22.png', dpi=500)
fig.savefig('../../plots/fig32.png', dpi=500)


# Figure 2.3
# %%
eps, cvar_eps, eta, cvar_eta = disturbance_smoothing(data)

fig, axs = plt.subplots(2,2, figsize=(12,7), sharex=True)
axs[0,0].set_title('i)')
axs[0,0].plot(eps, c='black', lw=.9)
axs[0,0].axhline(0, c='grey')
axs[0,1].set_title('ii)')
axs[0,1].plot(np.sqrt(cvar_eps), c='black')
axs[1,0].set_title('iii)')
axs[1,0].plot(eta, c='black', lw=.9)
axs[1,0].axhline(0, c='grey')
axs[1,1].set_title('iv)')
axs[1,1].plot(np.sqrt(cvar_eta), c='black')
fig.tight_layout()
fig.savefig('../../plots/fig23.png', dpi=500)
# fig.savefig('../../plots/fig33.png', dpi=500)


# Figure 2.5
# %%
data_new = data.copy()
# replicate missing data from the book
missing = np.append(np.arange(20,40), np.arange(60,80))
# # for external dataset
# missing = np.append(np.arange(10,20), np.arange(35,45))
data_new.iloc[missing] = np.nan

v, F, K, a, P = kalman_filter(data_new)
r, alpha, N, V = state_smoothing(data_new)

fig, axs = plt.subplots(2,2, figsize=(12,7), sharex=True)
axs[0,0].set_title('i)')
axs[0,0].plot(data_new, c='black', lw=.9)
axs[0,0].plot(a.shift(-1), c='grey', lw=.9)
axs[0,1].set_title('ii)')
axs[0,1].plot(P[1:], c='black', lw=1)
axs[1,0].set_title('iii)')
axs[1,0].plot(data_new, c='black', lw=.9)
axs[1,0].plot(alpha, c='grey', lw=.9)
axs[1,1].set_title('iv)')
axs[1,1].plot(V[1:], c='black', lw=1)
fig.tight_layout()
fig.savefig('../../plots/fig25.png', dpi=500)
#fig.savefig('../../plots/fig35.png', dpi=500)


# Figure 2.6
# %%
# we simply treat the dates that we want to forecast as missing
idx = data.index.append(pd.to_datetime(range(1971,2001), format='%Y').year)
# by appending it to original data
data_fct = pd.Series(np.append(data.values, [np.nan for i in range(30)]), index=idx)

v, F, K, a, P = kalman_filter(data_fct)
ci = .5*np.sqrt(F[-30:])

fig, axs = plt.subplots(2,2, figsize=(12,7), sharex=True)

axs[0,0].set_title('i)')
axs[0,0].plot(data_fct, '.', c='black')
axs[0,0].plot(a[1:], c='grey', lw=1.1)
axs[0,0].plot(a[100:] + ci, c='lightgrey', lw=.9)
axs[0,0].plot(a[100:] - ci, c='lightgrey', lw=.9)
axs[0,1].set_title('ii)')
axs[0,1].plot(P[1:], c='black', lw=1)
axs[1,0].set_title('iii)')
axs[1,0].plot(a[1:], c='black', lw=.9)
axs[1,1].set_title('iv)')
axs[1,1].plot(F[1:], c='black', lw=1)

fig.tight_layout()
fig.savefig('../../plots/fig26.png', dpi=500)

# # for external dataset
# idx = data.index.append(pd.to_datetime(range(2017,2031), format='%Y').year)
# data_fct = pd.Series(np.append(data.values, [np.nan for i in range(14)]), index=idx)

# v, F, K, a, P = kalman_filter(data_fct)
# ci = .5*np.sqrt(F[-14:])

# fig, axs = plt.subplots(2,2, figsize=(12,7), sharex=True)
# axs[0,0].set_title('i)')
# axs[0,0].plot(data_fct, '.', c='black')
# axs[0,0].plot(a[1:], c='grey', lw=1.1)
# axs[0,0].plot(a[56:] + ci, c='lightgrey', lw=.9)
# axs[0,0].plot(a[56:] - ci, c='lightgrey', lw=.9)
# axs[0,1].set_title('ii)')
# axs[0,1].plot(P[1:], c='black', lw=1)
# axs[1,0].set_title('iii)')
# axs[1,0].plot(a[1:], c='black', lw=.9)
# axs[1,1].set_title('iv)')
# axs[1,1].plot(F[1:], c='black', lw=1)
# fig.tight_layout()
# fig.savefig('../../plots/fig36.png', dpi=500)


# Figure 2.7
# %%
# initialize and optimize parameters of interest (variances of epsilon and eta)
ini = np.array([10000,1000])
results = minimize(llf, ini, data, method='SLSQP', 
                   bounds=((1,1000000),(1,1000000)), options={'disp': False})

var_eps, var_eta = results.x
v, F, K, a, P = kalman_filter(data, var_eps, var_eta)
e = v[1:] / np.sqrt(F[1:])

fig, axs = plt.subplots(2,2, figsize=(12,7))
axs[0,0].set_title('i)')
axs[0,0].plot(e, color='black', lw=.9)
axs[0,0].axhline(0, color='grey', lw=.9)
sns.distplot(e, hist=True, kde=True,  bins=12, color='black', hist_kws={'edgecolor':'black'}, ax=axs[0,1])
axs[0,1].set_title('ii)')
axs[0,1].set_ylabel('')
sm.qqplot(e, ax = axs[1,0], line ='s')
axs[1,0].set_title('iii)')
axs[1,0].set_xlabel('')
axs[1,0].set_ylabel('')
axs[1,0].get_lines()[0].set_color('black')
axs[1,0].get_lines()[0].set_markersize(1.5)
axs[1,0].get_lines()[1].set_color('grey')
axs[1,1].set_title('iv)')
axs[1,1].bar(range(1,11), acf(e,11)[1:], color='grey', edgecolor='black', width=.5)
axs[1,1].axhline(0, color='black', lw=.9)
axs[1,1].xaxis.set_ticks(np.arange(0, 11))
axs[1,1].yaxis.set_ticks(np.arange(-1, 1.5, 0.5))
axs[1,1].set_ylim([-1,1])
fig.tight_layout()
fig.savefig('../../plots/fig27.png', dpi=500)
# fig.savefig('../../plots/fig37.png', dpi=500)

# according to Karim's announcement, apply correction using constant terms
print(f'LLV:\t {-results.fun+data.size/2*np.log(2*np.pi) + (data.size-1)/2}')
results

# Figure 2.8
# %%
eps, cvar_eps, eta, cvar_eta = disturbance_smoothing(data, var_eps, var_eta)
# standardize
u_star = eps/np.std(eps)
r_star = eta/np.std(eta)

fig, axs = plt.subplots(2,2, figsize=(12,7))#, sharex=True)
axs[0,0].set_title('i)')
axs[0,0].plot(u_star, color='black', lw=.9)
axs[0,0].axhline(0, color='grey', lw=.9)
sns.distplot(u_star, hist=True, kde=True,  bins=12, color='black', hist_kws={'edgecolor':'black'}, ax=axs[0,1])
axs[0,1].set_title('ii)')
axs[0,1].set_ylabel('')
axs[1,0].set_title('iii)')
axs[1,0].plot(r_star, color='black', lw=.9)
axs[1,0].axhline(0, color='grey', lw=.9)
sns.distplot(r_star, hist=True, kde=True,  bins=12, color='black', hist_kws={'edgecolor':'black'}, ax=axs[1,1])
axs[1,1].set_title('iv)')
axs[1,1].set_ylabel('')
fig.tight_layout()
fig.savefig('../../plots/fig28.png', dpi=500)
# fig.savefig('../../plots/fig38.png', dpi=500)