# %%
import warnings
warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from tabulate import tabulate
from scipy.optimize import minimize
from scipy.stats import skew
from scipy.stats import kurtosis
from scipy.stats import norm
from numpy import dot, transpose, log, pi, exp
from numpy import random as r
pd.options.display.float_format = '{:.4f}'.format

# import
fx = pd.read_pickle('../../data/sv.pkl')
# scale back to original
fx_rescaled = fx/100
# linearize
fx_lin = np.log( (fx_rescaled-fx_rescaled.mean())**2 )

# %%
def plot_density(data1, data2):
    fig, axs = plt.subplots(1,2, figsize=(10,3))
    sns.distplot(data1, hist=True, kde=True,  bins=20, color='black', hist_kws={'edgecolor':'black'}, ax=axs[0])
    axs[0].set_title('i)')
    axs[0].set_xlabel(r'')
    axs[0].set_ylabel('')
    sns.distplot(data2, hist=True, kde=True,  bins=20, color='black', hist_kws={'edgecolor':'black'}, ax=axs[1])
    axs[1].set_title('ii)')
    axs[1].set_xlabel(r'')
    axs[1].set_ylabel('')
    fig.tight_layout()
    return fig

def table_descriptives(data, cols=['x', 'x_rescaled', 'x_linear']):
    df = pd.DataFrame(columns=cols, index=['count', 'mean', 'variance', 'skewness', 'kurtosis'])
    df.loc['count'] = len(data)
    df.loc['mean'] = data.mean().values
    df.loc['variance'] = data.var().values
    df.loc['skewness'] = [skew(data.iloc[:,i].dropna()) for i in range(len(data.columns))]
    df.loc['kurtosis'] = [kurtosis(data.iloc[:,i].dropna()) for i in range(len(data.columns))]
    return df

def plot_fig1(arr1, arr2):
    fig, axs = plt.subplots(2,1,figsize=(10,6), sharex=True)
    axs[0].set_title('i)')
    axs[0].plot(arr1, c='black', lw=.8)
    axs[0].set_yticks([-0.02, 0, 0.02, 0.04])
    axs[1].set_title('ii)')
    axs[1].plot(arr2, c='black', lw=.8)
    fig.tight_layout()
    return fig

fig_density = plot_density(fx_rescaled, fx_lin)
fig_density.savefig('../../plots/ass2/fig_des1.png', dpi=500)

fig1 = plot_fig1(fx_rescaled, fx_lin)
fig1.savefig('../../plots/ass2/fig1.png', dpi=500)

df = table_descriptives( pd.concat([fx, fx_rescaled, fx_lin], axis=1))
print(f'\n {df} \n')

# %%
def plot_fig(data, model):

    omega, phi, var_eta = model.params
    xi = omega / (1-phi)

    fig, axs = plt.subplots(2,1,figsize=(10,6), sharex=True)
    # first figure with data and smoothedestate h_t
    axs[0].set_title('i)')
    axs[0].plot(data, 'bo', markersize=2, c='darkgrey')
    axs[0].plot(model.state_smoothed[1:], c='black', lw='2')
    # second figure with smoothed and filtered state H_t
    filtered = model.state_filtered - xi
    smoothed = model.state_smoothed - xi
    axs[1].set_title('ii)')
    axs[1].plot(filtered, c='grey')
    axs[1].plot(smoothed[1:], c='black')
    fig.tight_layout()
    return fig

# %%
class StochasticVolatility():

    '''
    base class for stochastic volatility model in linear state space form
    note: make sure that input variables x and exog are transformed already (demeaned, log, etc..)

    :params:
        - x : array
            observation vector
        - exog : array
            optional if e.g. realized variance is added as regerssor in observation equation
    '''

    def __init__(self, y, exog=None):
        # attributes
        self.y = y
        self.nobs = len(self.y)
        # parameters to be optimized
        self._params_names = ['omega', 'phi', 'var_eta']
        self._params_ini   = [-0.12, 0.98, 0.008]
        self._bounds=((-5, 0.999), (0, 1), (0.00001, 100))
        # define fixed values
        self.d = -1.27
        self.var_eps = (np.pi**2)/2
        # for realized variance
        if exog is not None:
            self.exog = exog
            self._params_names.append('beta')
            self._params_ini.append(.8)
            self._bounds = ((-0.999, 0.999), (0, 1), (0.00001, 1), (-1,1))
        else:
            self.exog = np.zeros(y.size)

    def _kf(self, params, purpose='optim'):

        # unpack params
        n = self.nobs
        # if rv is added
        if np.sum(self.exog) != 0:
            omega, phi, var_eta, beta = params
            c = np.array([[omega, 0]]).T
            T = np.array([[phi, 0],
                            [0, 1]])
            R = np.array([[1, 0]]).T
            # create empty arrays
            a, K = [np.zeros((n,2,1)) for i in range(2)]
            P = np.zeros((n,2,2))
        else:
            omega, phi, var_eta = params
            c = np.array([[omega]])
            T = np.array([[phi]])
            R = np.array([[1]])
            Z = np.array([[1]])
            # create empty arrays
            a, P, K = [np.zeros(self.y.size) for i in range(3)]
        # general
        v, F = [np.zeros(self.y.size) for i in range(2)]
        d = self.d
        H = self.var_eps
        Q = var_eta
        a1 = omega /( 1-phi)
        P1 = var_eta / (1-phi**2)

        # RECURSIONS
        for t in range(n):
            # if rv
            if np.sum(self.exog) != 0:
                Z = np.array([[1, self.exog[t]]])
            # initialize
            if t == 0:
                # if rv is added
                if np.sum(self.exog) != 0:
                    a[t] = c + np.array([[a1, beta]]).T
                    P[t] = np.array([[P1, 0], [0, 0]])
                else:
                    a[t] = np.array([a1])
                    P[t] = np.array([P1])
                v[t] = self.y[t] - dot(Z, a[t]) - d
                F[t] = np.array( dot(dot(Z, P[t]), Z.T) + H )
                K[t] = dot(dot(dot(T, P[t]), Z.T), 1/F[t])
            else:
                # state update
                a[t] = c + dot(T, a[t-1]) + dot(K[t-1], v[t-1])
                P[t] = dot(dot(T, P[t-1]), T.T) + dot(dot(R, Q), R.T) - dot(dot(K[t-1], F[t-1]), K[t-1].T)
                v[t] = self.y[t] - dot(Z, a[t]) - d
                F[t] = dot(dot(Z, P[t]), Z.T) + H
                K[t] = dot(dot(dot(T, P[t]), Z.T), 1/F[t])

        if purpose == 'optim':
            return v, F
        if purpose == 'smooth':
            return a, P, v, F, K, T
        if purpose == 'set values':
            self.state_filtered = a

    def _kfs(self, params):

        n = self.nobs
        a, P, v, F, K, T = self._kf(params, purpose='smooth')
        # if rv is added
        if np.sum(self.exog) != 0:
            alpha, r = [np.zeros((n,2,1)) for i in range(2)]
            N, V = [np.zeros((n,2,2)) for i in range(2)]
            T = np.array([[params[1], 0], [0, 1]])
        else:
            r, alpha, N, V = [np.zeros(n) for i in range(4)]
            Z = np.array([[1]])
            Lt = params[1] - K

        # RECURSION
        for t in range(n-1, 0, -1):
                if np.sum(self.exog) != 0:
                    Z = np.array([[1, self.exog[t]]])
                    L = T - dot(K[t], Z)
                else:
                    L = np.array([[Lt[t]]])
                # calculate cumulant variables
                r[t-1]   = dot(dot(Z.T, 1/F[t]), v[t]) + dot(L.T, r[t])
                N[t-1]   = dot(dot(Z.T, 1/F[t]), Z)    + dot(dot(L.T, N[t]), L)
                # smoothed state update
                alpha[t] = a[t] + dot(P[t], r[t-1])
                V[t]     = P[t] - dot(dot(P[t], N[t-1]), P[t])

        # set values
        self.state_smoothed = alpha

    def _llf(self, params, output='optim'):
        # filter
        v, F = self._kf(params)
        # calculate log likelihood values
        ll = -(self.y.size/2)*np.log(2*np.pi) - 0.5*np.sum( np.log(F[1:]) + (v[1:]**2)/F[1:] )
        if output == 'optim':
            return -ll

    def _show_results(self):
        print('------------------------')
        print(tabulate({"Parameter": self._params_names,
                        "Estimate" : self.params}, headers="keys"))
        print('------------------------')
        print(f'LLV: {np.round(self._llv, 2)} ')
        print('\n\n')

    def fit(self, mthd='Nelder-Mead'):
        # optimisation
        res = minimize(self._llf, self._params_ini, bounds=self._bounds,
                        method = mthd, options={'disp': False})
        # assign estimates
        self.params = res.x
        self._kf(self.params, purpose='set values')
        self._kfs(self.params)
        self._llv = -res.fun
        self._show_results()

# %%
# fit model
model1 = StochasticVolatility(fx_lin)
model1.fit()

fig2 = plot_fig(fx_lin, model1)
fig2.savefig('../../plots/ass2/fig2.png', dpi=500)

# %%
# EXTENSION e)
import yfinance as yf
import datetime as dt
today = dt.date.today()

# import data
aex = yf.Ticker('^AEX')
aex = aex.history(start="2015-01-01", end=today)['Close']
rv  = pd.read_pickle('../../data/aex_rv_parzen.pkl')
# merge
df  = aex.to_frame().join(rv).dropna()
# calculate returns
aex_ret = np.log(df['Close']).diff().dropna()
rv  = df['rk_parzen'][1:]
# prepare for class
aex_lin = np.log( (aex_ret-aex_ret.mean())**2 )
rv_log  = np.log(rv)

# %%
# descriptives table
df = pd.concat([aex, aex_ret, aex_lin, rv, rv_log], axis=1)
df_des = table_descriptives(data=df,
                      cols=['aex price level', 'aex log return','aex log returns linearized',
                            'rv', 'log rv'])
print(df_des)

# %%
# plots
def plot_fig3(arr1, arr2, arr3, arr4):
    fig, axs = plt.subplots(2,2,figsize=(10,6), sharex=True)
    axs[0,0].set_title('i)')
    axs[0,0].plot(arr1, c='black', lw=.8)
    axs[0,0].set_yticks([700, 600, 500, 400])
    axs[0,1].set_title('ii)')
    axs[0,1].plot(arr2, c='black', lw=.8)
    axs[0,1].set_yticks([0.1, 0.05, 0, -0.05, -0.1])
    axs[1,0].set_title('iii)')
    axs[1,0].plot(arr3, c='black', lw=.8)
    axs[1,1].set_title('iv)')
    axs[1,1].plot(arr4, c='black', lw=.8)
    fig.tight_layout()
    return fig

fig = plot_fig3(aex, aex_ret, aex_lin, rv_log)
fig.savefig('../../plots/ass2/fig3.png', dpi=500)

# %%
# density plots
fig_density = plot_density(aex_ret, aex_lin)
fig_density.savefig('../../plots/ass2/fig_des2.png', dpi=500)

# %%
aex_lin = np.array(aex_lin)
rv_log  = np.array(rv_log)

# fit model without RV
model2 = StochasticVolatility(aex_lin)
model2.fit()

fig4 = plot_fig(aex_lin, model2)
fig4.savefig('../../plots/ass2/fig4.png', dpi=500)


# %%
# fit model with RV
model3 = StochasticVolatility(aex_lin, exog=rv_log)
model3.fit()

beta = model3.params[3]
h = [model3.state_filtered[i][0][0] for i in range(aex_lin.size)]
h_hat = [model3.state_smoothed[i][0][0] for i in range(aex_lin.size)][1:]

# plot
fig5, axs = plt.subplots(2,1,figsize=(10,6), sharex=True)
# the model equation
x = beta*rv_log[1:] + h_hat
axs[0].set_title('i)')
axs[0].plot(aex_lin, 'bo', markersize=2, c='darkgrey')
axs[0].plot(x,c='black', lw='1')
# account for constant
xi   = model3.params[0] / (1-model3.params[1])
axs[1].set_title('ii)')
axs[1].plot(h-xi,c='grey')
axs[1].plot(h_hat-xi,c='black')
fig5.tight_layout()
fig5.savefig('../../plots/ass2/fig5.png', dpi=500)

# %%
def table_estimates(model):
     return pd.DataFrame({'Estimate': list(model.params) + [model._llv] },index = model._params_names + ['llv'])

# %%
print('model 1')
print(table_estimates(model1).to_latex())
# %%
print('model 2')
print(table_estimates(model2).to_latex())
# %%
print('model 3')
print(table_estimates(model3).to_latex())

# %%
# EXTENSION f)
def bpf(data, params, N=10000):
    '''
    bootstrap particle filter for sv
    note: steps according to slides 24/25 from working group week 5 (Janneke)
    '''

    # unpack
    y = np.array(data)
    mu = np.mean(y)
    omega, phi, var_eta = params
    a1 = omega / (1-phi)
    P1 = var_eta / (1-phi**2)
    # create empty arrays
    H, ess = [np.zeros(y.size) for i in range(2)]
    pf = np.zeros((y.size, N))

    # bootstrap filter using loop
    for t in range(y.size):
        if t == 0:
            # initialize
            pf[t,:] = r.normal(a1, P1, N)
            H[t] = np.mean(pf[t,:])
        else:
            # sample
            a_tilde = omega + phi*pf[t-1,:] + r.normal(0, 1, size=N)*np.sqrt(var_eta)
            pf[t,:] = a_tilde
            # calculate weights
            w_tilde = exp( -1/2*(log(2*pi) + a_tilde + (y[t]-mu)**2 / exp(a_tilde)) )
            # normalize
            w = w_tilde / np.sum(w_tilde)
            # calculate mean
            H[t] = np.sum(w*a_tilde)
            ess[t] = np.sum(w**2)
            # resample
            rs = r.choice(range(N), size=N, replace=True, p=w)
            pf = pf[:,rs]
    return H, 1/ess

# given data
y_fx = pd.read_pickle('../../data/sv.pkl') / 100
H_fx, ess_fx = bpf(y_fx, model1.params)

# own data
y_aex = np.array(aex_ret)
H_aex, ess_aex = bpf(y_aex, model2.params)

# %%
def plot_bpf(model1, H1, model2, H2):
    fig, axs = plt.subplots(2,1, figsize=(10,6))
    xi1 = model1.params[0] / (1-model1.params[1])
    axs[0].set_title('i)')
    axs[0].plot(model1.state_filtered-xi1, c='grey', lw=2.5, alpha=.8)
    axs[0].set_yticks([-1,0,1])
    axs[0].plot(H1-xi1, c='black', lw=1)
    xi2 = model2.params[0] / (1-model2.params[1])
    axs[1].set_title('ii)')
    axs[1].plot(model2.state_filtered-xi2, c='grey', lw=2.5, alpha=.8)
    axs[1].set_yticks([-1,0,1, 2])
    axs[1].plot(H2-xi2, c='black', lw=1)
    fig.tight_layout()
    return fig

fig6 = plot_bpf(model1, H_fx, model2, H_aex)
fig6.savefig('../../plots/ass2/fig6.png', dpi=500)

# %%
# ESS
def plot_ess(data1, data2):
    fig, axs = plt.subplots(1,2, figsize=(10,3))
    axs[0].set_title('i)')
    axs[0].plot(ess_fx, c='black')
    axs[1].set_title('ii)')
    axs[1].plot(ess_aex, c='black')
    fig.tight_layout()
    return fig

fig = plot_ess(ess_fx, ess_aex)
fig.savefig('../../plots/ass2/fig_ess.png', dpi=500)

# %%
def plot_additional(data1,data2):
    fig,ax=plt.subplots(figsize=(10,3))
    ax.set_yticks([-12,-10,-8,-6])
    ax.set_title('i)')
    ax.plot(data1, c='grey')
    ax.plot(data2, c='black')
    fig.tight_layout()
    return fig

fig = plot_additional(x, model2.state_smoothed[1:])
fig.savefig('../../plots/ass2/fig_addiotnal.png', dpi=500)
