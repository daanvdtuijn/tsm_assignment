# %%
import pandas as pd
import numpy as np
import seaborn as sns
import yfinance as yf
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import minimize
import statsmodels.api as sm
from numpy.linalg import inv
from numpy import dot
from numpy import transpose
import scipy.linalg as linalg


# read data
data = pd.read_pickle("../../data/Nile_daan.pkl")
# set global constants
var_eps = np.array(15099)
var_eta = np.array(1469.1)
# initial values
a_ini = np.array(0)
P_ini = np.array(10**7)

# %% 
# previous function
def kalman_filter(data, var_eps=var_eps, var_eta=var_eta, a_ini=a_ini, P_ini=P_ini):
    ''' used to filter data using the recursions as discussed in the report''' 
    
    y = np.asarray(data)
    a, K, v, F, P = [np.zeros(y.size) for i in range(5)] 
    # initialize
    a[0] = a_ini
    P[0] = P_ini
    v[0] = y[0] - a[0]
    F[0] = P[0] + var_eps
    K[0] = P[0] / F[0]

    # recursions
    for t in range(1, y.size):
        # if data is missing
        if np.isnan(y[t-1]):
            a[t] = a[t-1]
            P[t] = P[t-1] + var_eta
            F[t] = P[t]   + var_eps
        else:
            # state update
            a[t] = a[t-1] + K[t-1]*v[t-1]
            P[t] = P[t-1]*(1-K[t-1]) + var_eta
            v[t] = y[t] - a[t]
            F[t] = P[t] + var_eps
            K[t] = P[t] / F[t]
        
    return [pd.Series(i, index=data.index) for i in [v, F, K, a, P]]

v, F, K, a, P = kalman_filter(data)

plt.plot(data)
plt.plot(a)

# # %%
# # new function
# def kf(data, Z=np.array(1), T=np.array(1), R=np.array(1), H=var_eps, Q=var_eta, a_ini=a_ini, P_ini=P_ini):

#     y = np.asarray(data)
#     a, K, v, F, P = [np.zeros(y.size) for i in range(5)]

#     # for beta thing
#     Z = np.tile([[Z]], y.size).T

#     # initialize
#     a[0] = a_ini
#     P[0] = P_ini
#     v[0] = y[0] - Z[0].dot(a[0])
#     F[0] = np.array( dot(Z[0].dot(P[0]), transpose(Z[0])) + H )
#     # exception because it's not possible to take inverse of scalar
#     try:
#         K[0] = dot(dot(dot(T, (P[0])), transpose(Z[0])), inv(F[0]))
#     except:
#         K[0] = dot(dot(dot(T, (P[0])), transpose(Z[0])), 1/F[0])

#     # looping over y to apply recursions
#     for t in range(1, y.size):
#         # if data is missing

#         else:
#             # state update
#             a[t] = dot(T, a[t-1]) + dot(K[t-1], v[t-1])
#             P[t] = dot(dot(T, P[t-1]), T.T) + dot(dot(R, Q), R.T) - dot(dot(K[t-1], F[t-1]), K[t-1].T)
#             v[t] = y[t] - dot(Z[t], a[t])
#             F[t] = dot(dot(Z[t], P[t]), transpose(Z[t])) + H
#             # exception because it's not possible to take inverse of scalar
#             try:
#                 K[t] = dot(dot(dot(T, P[t]), transpose(Z[t])), inv(F[t]))
#             except:
#                 K[t] = dot(dot(dot(T, P[t]), transpose(Z[t])), 1/F[t])



# %%
def kf(data):

    # unpack params
    y = np.array(data)
    n = y.size
    d = np.array([[0]])
    T = np.array([[1]])
    R = np.array([[1]])
    c = np.array([[0]])
    Z = np.array([[1]])
    H = var_eps
    Q = var_eta

    # create empty arrays
    a, P, K = [np.zeros(n) for i in range(3)]
    v, F = [np.zeros(n) for i in range(2)]
    
    # RECURSIONS
    for t in range(n):
        # initialize
        if t == 0:
            a[t] = np.array([[0]])
            P[t] = np.array([[10**7]])
            v[t] = y[t] - dot(Z, a[t]) - c 
            F[t] = np.array( dot(dot(Z, P[t]), Z.T) + H )
            K[t] = dot(dot(dot(T, P[t]), Z.T), 1/F[t]) 
        else:
            # state update
            a[t] = d + dot(T, a[t-1]) + dot(K[t-1], v[t-1])
            P[t] = dot(dot(T, P[t-1]), T.T) + dot(dot(R, Q), R.T) - dot(dot(K[t-1], F[t-1]), K[t-1].T)
            v[t] = y[t] - dot(Z, a[t])
            F[t] = dot(dot(Z, P[t]), Z.T) + H
            K[t] = dot(dot(dot(T, P[t]), Z.T), 1/F[t])
    
    return v, F, K, a, P
    
v, F, K, a, P = kf(data)


# %%
def kfs(data):
    n = data.size
    v, F, K, a, P = kf(data)
    y = np.asarray(data)

    # 2x2 and 2x1 matrices if rv
    r, alpha, N, V = [np.zeros(n) for i in range(4)]

    T, Z = 1, 1
    L = T - dot(K, Z)

     # recursion 
    for t in range(n-1, 0, -1):
            # if rv: Z = ..
            # calculate cumulant variables
            r[t-1]   = dot(dot(transpose(Z), 1/F[t]), v[t]) + L[t]*r[t]
            N[t-1]   = dot(dot(transpose(Z), 1/F[t]), Z) + dot(dot(transpose(L[t]), N[t]), L[t])
            # smoothed state update
            alpha[t] = a[t] + P[t]*r[t-1]
            V[t]     = P[t] - dot(dot(P[t], N[t-1]), P[t])
            # last loop
            if t == 1:
                # last loop to make length of vectors 101
                r = np.insert(r, 0, dot(dot(transpose(Z), 1/F[0]), v[0]) + L[0]*r[0])
                N = np.insert(N, 0, dot(dot(transpose(Z), 1/F[0]), Z) + dot(dot(transpose(L[0]), N[0]), L[0]))
                alpha[0] = a[0] + P[0]*r[0]
                V[0]     = P[0] - dot(dot(P[0], N[0]), P[0])
                # again; 2x1 and 2x2 matrices if rv
                alpha    = np.insert(alpha, 0, np.nan)
                V        = np.insert(V, 0, np.nan)
    return alpha, V, r, N 

alpha, V, r, N = kfs(data)
plt.plot(data.values)
plt.plot(a)
plt.plot(alpha)
alpha2=alpha
# # %%


#     ''' used to smooth the state using the backward recursions as discussed in the report'''



#     # recursion 
#     for t in range(y.size-1, 0, -1):
#             # calculate cumulant variables
#             r[t-1]   = v[t]/F[t] + L[t]*r[t]
#             N[t-1]   = 1/F[t] + (L[t]**2)*N[t]
#             # smoothed state update
#             alpha[t] = a[t] + P[t]*r[t-1]
#             V[t]     = P[t] - (P[t]**2)*N[t-1]
#     # last loop to make length of vectors 101
#     # r = np.insert(r, 0, v[t-1]/F[t-1] + L[t-1]*r[t-1])
#     # N = np.insert(N, 0, 1/F[t-1] + (L[t-1]**2)*N[t-1])
#     # alpha[0] = a[0] + P[0]*r[0]
#     # V[0]     = P[0] - (P[0]**2)*N[0]
#     # alpha    = np.insert(alpha, 0, np.nan)
#     # V        = np.insert(V, 0, np.nan)

# %%
def state_smoothing(data, var_eps=var_eps, var_eta=var_eta, a_ini=a_ini, P_ini=P_ini):
    ''' used to smooth the state using the backward recursions as discussed in the report'''

    y = np.asarray(data)
    # first apply kalman filter from above on data
    v, F, K, a, P = kf(data)
    r, alpha, N, V = [np.zeros(y.size) for i in range(4)]
    L = 1 - K

    # recursion 
    for t in range(y.size-1, 0, -1):
        # if data is missing
        if np.isnan(y[t-1]):
            # take previous value
            r[t-1]   = r[t]
            N[t-1]   = N[t]
            # smoothed state update
            alpha[t] = a[t] + P[t]*r[t-1]
            V[t]     = P[t] - (P[t]**2)*N[t-1]
        else:
            # calculate cumulant variables
            r[t-1]   = v[t]/F[t] + L[t]*r[t]
            N[t-1]   = 1/F[t] + (L[t]**2)*N[t]
            # smoothed state update
            alpha[t] = a[t] + P[t]*r[t-1]
            V[t]     = P[t] - (P[t]**2)*N[t-1]
    print(t)
    # last loop to make length of vectors 101
    r = np.insert(r, 0, v[t-1]/F[t-1] + L[t-1]*r[t-1])
    N = np.insert(N, 0, 1/F[t-1] + (L[t-1]**2)*N[t-1])
    alpha[0] = a[0] + P[0]*r[0]
    V[0]     = P[0] - (P[0]**2)*N[0]
    alpha    = np.insert(alpha, 0, np.nan)
    V        = np.insert(V, 0, np.nan)

    return [pd.Series(i, index=data.index.insert(0, pd.to_datetime(int(data.index[0])-1, format='%Y').year)) for i in [r, alpha, N, V]]
# %%
r, alpha, N, V = state_smoothing(data)
# %%

# %%

# week 5: slide 8 
S = 10000
draws = np.random.uniform(1,2, size=S)
1/S * np.sum( np.exp(draws) )
# %%

