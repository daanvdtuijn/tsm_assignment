import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from statsmodels.graphics import tsaplots
from scipy.optimize import minimize
import statsmodels.api as sm
import seaborn as sb

# Given parameters
sig_eps = 15099
sig_eta = 1469.1

# Original dataset
nile_data = pd.read_csv('nile.csv')
y = list(nile_data.iloc[:,0])

# Altered datasets
#y_forecast = y + [np.nan for t in range(30)]

def missing_obs():
    for  t in range(20, 40):
        y[t] = np.nan
    for t in range(60, 80):
        y[t] = np.nan
    return y
#y_missing = missing_obs()

'''Weet nog niet hoe ik v[t] moet fixen voor missing observations'''
def kalman_filter(a1, P1, sig_eps, sig_eta, y=list(nile_data.iloc[:,0])):
    n = len(y)
    a = np.zeros(n)
    P = np.zeros(n)
    v = np.zeros(n)
    F = np.zeros(n)
    K = np.zeros(n)

    a[0] = a1
    P[0] = P1
    v[0] = y[0] - a[0]
    F[0] = P[0] + sig_eps
    K[0] = P[0] / F[0]

    for t in range(1, n):
        if math.isnan(y[t-1]):
            a[t] = a[t-1]
            P[t] = P[t-1] + sig_eta
            #v[t] = x[t] + eps_hat[t] Weet nog niet hoe ik dit moet fixen
            # Dus alpha[t] and V[t] kloppen nog niet voor missing observations
        else:
            a[t] = a[t-1] + (P[t-1] * (y[t-1] - a[t-1])) / (P[t-1] + sig_eps)
            P[t] = P[t-1] * sig_eps / (P[t-1] + sig_eps) + sig_eta
            v[t] = y[t] - a[t]

        F[t] = P[t] + sig_eps
        K[t] = P[t] / F[t]

    return a, P, F, v, K

a, P, F, v, K = kalman_filter(0, 10**7, sig_eps, sig_eta)

def kalman_smoother(a, P, F, v, sig_eps, y=list(nile_data.iloc[:,0])):
    L = np.zeros(len(y))
    V = np.zeros(len(y))
    N = np.zeros(len(y))
    r = np.zeros(len(y))
    alpha = np.zeros(len(y))

    for t in range(len(y)):
        L[t] = sig_eps / F[t]

    for t in range(len(y) - 2, 0, -1):
        r[t] = v[t + 1] / F[t + 1] + L[t + 1] * r[t + 1]
        N[t] = 1 / F[t + 1] + N[t + 1] * L[t + 1] ** 2

    for t in range(1, len(y)):
        V[t] = P[t] - N[t-1] * P[t] ** 2
        alpha[t] = a[t] + P[t] * r[t - 1]

    return L, V, N, r, alpha

L, V, N, r, alpha = kalman_smoother(a, P, F, v, sig_eps)

def disturbance_smoothing(F, P, alpha, r, N, y=list(nile_data.iloc[:,0])):
    eta_hat = np.zeros(len(y))
    eta_std = np.zeros(len(y))
    eps_hat = np.zeros(len(y))
    eps_std = np.zeros(len(y))

    for t in range(len(y)):
        eta_hat[t] = sig_eta * r[t]
        eta_std[t] = np.sqrt(sig_eta - N[t] * sig_eta ** 2)
        eps_hat[t] = y[t] - alpha[t]
        eps_std[t] = np.sqrt(sig_eps - sig_eps ** 2 * (1 / F[t] + (P[t] / F[t]) ** 2 * N[t]))

    return eta_hat, eta_std, eps_hat, eps_std

eta_hat, eta_std, eps_hat, eps_std = disturbance_smoothing(F, P, alpha, r, N)

'''MLE gaat nog niet goed'''
def parameter_estimation(params):

    n = len(y)
    a = np.zeros(n)
    P = np.zeros(n)
    v = np.zeros(n)
    F = np.zeros(n)
    loglikelihood = np.zeros(n)

    sig_eps = params[0]
    sig_eta = params[1]

    a[0] = 0
    P[0] = 10**7
    v[0] = y[0] - a[0]
    F[0] = P[0] + sig_eps
    loglikelihood[0] = np.log(F[0]) + v[0] ** 2 / F[0]

    for t in range(1, n):

        a[t] = a[t-1] + (P[t-1] * (y[t-1] - a[t-1])) / (P[t-1] + sig_eps)
        P[t] = P[t-1] * sig_eps / (P[t-1] + sig_eps) + sig_eta
        v[t] = y[t] - a[t]
        F[t] = P[t] + sig_eps
        print(sig_eps)
        loglikelihood[t] = np.log(F[t]) + v[t] ** 2 / F[t]

    term1 = n/2 * np.log(2 * np.pi)
    term2 = 1/2 * sum(loglikelihood)

    return  term1 + term2

def maximum_likelihood(initial_params, func):

    print(minimize(func, initial_params, method='BFGS'))


def diagnostic_checking(eps_hat, eps_std, eta_hat, eta_std, v, F, y=list(nile_data.iloc[:,0])):
    n = len(y)
    e = np.zeros(n)
    u = np.zeros(n)
    r = np.zeros(n)

    for t in range(n):
        e[t] = v[t]/ np.sqrt(F[t])
        u[t] = eps_hat[t] / eps_std[t]**2
        r[t] = eta_hat[t] / eta_std[t]**2

    # Plot residuals
    #plt.plot(e, linewidth=0.5)
    #plt.plot(u, linewidth=0.5)
    #plt.plot(r, linewidth=0.5)
    #plt.show()

    # Estimated density
    #sb.distplot(e, bins=11, kde=True)
    #sb.distplot(u, bins=11, kde=True)
    #sb.distplot(r, bins=11, kde=True)
    #plt.show()

    # QQ plot
    #fig = sm.qqplot(e, line='45')
    #plt.show()

    # Correlogram
    #fig = tsaplots.plot_pacf(e, lags=10)
    #plt.show()

    return True

diagnostic_checking(eps_hat, eps_std, eta_hat, eta_std, v, F)


#plt.scatter(range(len(y_forecast)-1), y_forecast[1:])
#plt.plot(y_forecast, linewidth=0.5)
#plt.plot(F[2:], linewidth=0.5)#plt.show()
#plt.show()

