import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import minimize

data = pd.read_csv('sv.csv')
returns = data.iloc[:,0]
############
### (a)
############
y = returns/100
y = y - np.mean(y)
y.describe()

#plt.plot(y, linewidth=0.5)
#plt.show()

############
### (b)
############
y = np.log(y**2)

#plt.scatter(range(len(y2)), y2, linewidth=0.5)
#plt.show()

############
### (c)
############

def kalman_filter(y, params):
    n = len(y)
    a = np.zeros(n)
    P = np.zeros(n)
    v = np.zeros(n)
    F = np.zeros(n)
    loglikelihood = np.zeros(n)

    omega = params[0]
    phi = params[1]
    Qt = params[2]
    sig_eps = 4.93

    a[0] = omega / (1 - phi)
    P[0] = sig_eps / (1 - phi ** 2)
    v[0] = y[0] - a[0]
    F[0] = P[0] + sig_eps
    loglikelihood[0] = np.log(F[0]) + v[0] ** 2 / F[0]

    for t in range(1, n):
        a[t] = omega + phi * a[t - 1] + (P[t - 1] * (y[t - 1] - a[t - 1])) / (P[t - 1] + sig_eps)
        P[t] = P[t - 1] * sig_eps / (P[t - 1] + sig_eps) + Qt
        v[t] = -1.27 + y[t] - a[t]
        F[t] = P[t] + sig_eps

    #a[0] = np.nan
    #P[0] = np.nan
    #F[0] = np.nan
    #v[0] = np.nan

    return a, P, F, v

a, P, F, v = kalman_filter(y, [-0.43966093,  0.96942009,  0.00757306])

def kalman_smoother(a, P, F, v, sig_eps, y):
    L = np.zeros(len(y))
    V = np.zeros(len(y)+1)
    N = np.zeros(len(y))
    r = np.zeros(len(y))
    alpha = np.zeros(len(y)+1)

    for t in range(len(y)):
        L[t] = sig_eps / F[t]

    for t in range(len(y) - 2, 0, -1):
        r[t] = v[t + 1] / F[t + 1] + L[t + 1] * r[t + 1]
        N[t] = 1 / F[t + 1] + N[t + 1] * L[t + 1] ** 2

    r[0] = v[1] / F[1] + L[1] * r[1]
    r = np.insert(r, 0, v[0] / F[0] + L[0] * r[0])

    N[0] = 1 / F[1] + N[1] * L[1] ** 2
    N = np.insert(N, 0, 1 / F[0] + N[0] * L[0] ** 2)

    P = np.insert(P, 0, np.nan)
    a = np.insert(a, 0, np.nan)

    for t in range(len(y)+1):
        V[t] = P[t] - N[t-1] * P[t] ** 2
        alpha[t] = a[t] + P[t] * r[t - 1]

    #alpha[0] = np.nan

    return L, V, N, r, alpha

L, V, N, r, alpha = kalman_smoother(a, P, F, v, 4.93, y)

def parameter_estimation(params):

    n = len(y)
    a = np.zeros(n)
    P = np.zeros(n)
    v = np.zeros(n)
    F = np.zeros(n)
    loglikelihood = np.zeros(n)

    omega = params[0]
    phi = params[1]
    Qt = params[2]
    sig_eps = 4.93

    a[0] = omega/(1-phi)
    P[0] = sig_eps/(1-phi**2)
    v[0] = y[0] - a[0]
    F[0] = P[0] + sig_eps
    loglikelihood[0] = np.log(F[0]) + v[0] ** 2 / F[0]

    for t in range(1, n):

        a[t] = omega + phi*a[t-1] + (P[t-1] * (y[t-1] - a[t-1])) / (P[t-1] + sig_eps)
        P[t] = P[t-1] * sig_eps / (P[t-1] + sig_eps) + Qt
        v[t] = -1.27 + y[t] - a[t]
        F[t] = P[t] + sig_eps
        loglikelihood[t] = np.log(F[t]) + v[t] ** 2 / F[t]

    term2 = n/2 * np.log(2*np.pi) + 1/2 * sum(loglikelihood)
    return term2

def maximum_likelihood(initial_params, func):

    print(minimize(func, initial_params, method='Nelder-Mead'))

#params = [0.2, 0.4, 0.8]
#maximum_likelihood(params, parameter_estimation)

def particle_filter(a1=np.mean(y), sig_eta=0.8035, sig_chi=4.93):

    N = 1000

    # Initialize
    sample = np.zeros(N)
    weights = np.zeros(N)
    a_hat = np.zeros(len(y))

    # First iteration
    for i in range(N):
        sample[i] = np.random.normal(a1, sig_eta)
        weights[i] = np.exp(-0.5 * np.log(2 * np.pi) - 0.5 * np.log(sig_chi) -0.5 * sig_chi**-2 * (y[0] - 1.27 - sample[i]))
    # Normalize weights
    for i in range(N):
        weights[i] = weights[i] / sum(weights)

    a_hat[0] = np.matmul(sample, weights)

    for t in range(1, len(y)):
        print (t)
        for i in range(N):
            sample[i] = np.random.normal(a_hat[t-1], sig_eta)
            weights[i] = weights[i]*np.exp(-0.5 * np.log(2 * np.pi) - 0.5 * np.log(sig_chi) -0.5 * sig_chi**-2 * (y[t] - 1.27 - sample[i]))

        for i in range(N):
            weights[i] = weights[i] / sum(weights)

        a_hat[t] = np.matmul(sample, weights)

    a_hat[0] = np.nan

    return a_hat

a_hat = particle_filter()

plt.scatter(range(len(y)), y)
plt.plot(a_hat, linewidth=1, color='black')
plt.show()

'''
nile_data = pd.read_csv('nile.csv')
y = list(nile_data.iloc[:,0])
# Given parameters
sig_eps = 15099
sig_eta = 1469.1
'''